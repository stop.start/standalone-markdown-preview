package main

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"fmt"
)

const (
	markdownExtension = ".md"
)

func createTempHTML(filePath string) string{
	filePathArr := strings.Split(filePath, "/")
	filename := filePathArr[len(filePathArr)-1]
	return fmt.Sprintf("%s/%s.html", tmpFolder, strings.TrimSuffix(filename, markdownExtension))
}

func isDirectory(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		log.Errorf("file %s was created but not found", path)
		return false
	}
	return fileInfo.IsDir()
}

func isMarkdown(file string) bool {
	return strings.HasSuffix(file, markdownExtension)
}

func isVisibleDir(file os.FileInfo) bool {
	return file.IsDir() && !strings.HasPrefix(file.Name(), ".")
}
