package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func server(path string, port int) {
	router := mux.NewRouter()
	router.HandleFunc(path, handleOutput()).Methods("GET")
	http.ListenAndServe(fmt.Sprintf(":%d", port), router)
}

func handleOutput() func(res http.ResponseWriter, req *http.Request) {
	return func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Content-Type", " text/html")
		res.WriteHeader(http.StatusOK)
		res.Write(htmlFileToServe)
	}
}
