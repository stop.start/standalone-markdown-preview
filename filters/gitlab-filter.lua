function Math(el)
    if el.text:sub(1,1) == '`' and el.text:sub(#el.text) == '`' then
            local text = el.text:sub(2,#el.text-1)
            return pandoc.Math("InlineMath", text)
    end
end

function CodeBlock(el)
    if el.classes[1] == "math" then
        return pandoc.Para({ pandoc.Math("DisplayMath", el.text) })
    end
end
