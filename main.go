package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"

	"strings"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/stop.start/go-chrome-remote-reload"
)

var htmlFileToServe []byte
var tmpFolder string
var filtersFolder string

func initWatcher(path string) (*fsnotify.Watcher, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, fmt.Errorf("error while creating watcher: %s", err)
	}

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, fmt.Errorf("error while reading the directory %s: %s", path, err)
	}

	err = watcher.Add(path)
	if err != nil {
		return nil, fmt.Errorf("error while adding %s to watcher: %s", path, err)
	}
	log.Infof("Added %s to watched folders", path)

	for _, file := range files {
		if isVisibleDir(file) {
			filePath := fmt.Sprintf("%s/%s", path, file.Name())
			err = watcher.Add(filePath)
			if err != nil {
				log.Warnf("error while adding subfolder %s to watcher: %s", filePath, err)
			}
			log.Infof("Added %s to watched folders", filePath)
		}
	}

	return watcher, nil
}

func watch(watcher *fsnotify.Watcher, rc *reloader.RemoteConfig, shutdown chan bool) {
	for {
		select {
		case <-shutdown:
			return
		case event := <-watcher.Events:
			switch event.Op {
			case fsnotify.Create:
				if isDirectory(event.Name) {
					if err := watcher.Add(event.Name); err != nil {
						log.Errorf("could not add %s to watcher: %s", event.Name, err)
					}
				}
			case fsnotify.Write:
				if isMarkdown(event.Name) {
					htmlFile, err := convertToHTML(event.Name)
					if err != nil {
						log.Error(err)
					}
					if err := updateViewer(htmlFile, rc); err != nil {
						log.Error(err)
					}
				}
			}
		case err := <-watcher.Errors:
			log.Errorf("error from watcher: %s", err)
		}
	}
}

func convertToHTML(fileName string) (string, error) {
	htmlFile := createTempHTML(fileName)
	// ---- TODO: Make the command an external parameter/config
	cmd := exec.Command("pandoc", "-s", "--lua-filter", fmt.Sprintf("%s/gitlab-filter.lua", filtersFolder), "--katex=https://cdn.jsdelivr.net/npm/katex@0.10.0-rc.1/dist/", "--css", "https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.css", "-o", htmlFile, fileName)
	if err := cmd.Run(); err != nil {
		return "", fmt.Errorf("could not update html with pandoc: %s\ncmd: pandoc -s --lua-filter %s/gitlab-filter.lua --katex https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.10.0-rc.1 -css https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.css -o %s %s", err, filtersFolder, htmlFile, fileName)
	}
	return htmlFile, nil
}

func updateViewer(fileName string, rc *reloader.RemoteConfig) error {
	htmlFileToServe, _ = ioutil.ReadFile(fileName)
	htmlFileToServe = []byte(strings.Replace(string(htmlFileToServe), "<body>", "<body class=\"markdown-body\">", -1))
	return rc.ReloadTab(rc.OriginRoute)
}

//------------------------------------------------------------------------------
func main() {

	app := cli.NewApp()
	app.Name = "cli"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "d, dir",
			Usage: "Directory to watch",
			Value: ".",
		},
		cli.StringFlag{
			Name:  "f, file",
			Usage: "first file to server",
		},
		cli.StringFlag{
			Name:  "b, bin",
			Usage: "brower to open: chrome, chromium",
			Value: "chromium",
		},
		cli.StringFlag{
			Name:  "r, route",
			Usage: "route where the html is previewed",
			Value: "/output",
		},
		cli.IntFlag{
			Name:  "p, port",
			Usage: "port for the local preview",
			Value: 8080,
		},
		cli.StringFlag{
			Name:        "t, tmp-dir",
			Usage:       "directory where temporary files are kept",
			Value:       "/tmp",
			Destination: &tmpFolder,
		},
		cli.StringFlag{
			Name:        "filters-dir",
			Usage:       "directory where temporary files are kept",
			Value:       fmt.Sprintf("%s/.standalone_markdown_preview/filters/", os.Getenv("HOME")),
			Destination: &filtersFolder,
		},
	}

	app.Action = func(c *cli.Context) error {
		// ---- SETTING LOGS
		log.SetFormatter(&log.TextFormatter{FullTimestamp: true, TimestampFormat: time.StampMilli, ForceColors: true})

		// ---- SETTING KILLING SIGNALS
		shutdown := make(chan bool)
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

		watcher, err := initWatcher(c.String("dir"))
		if err != nil {
			log.Fatalf("Could not initialize watcher: %s", err)
		}
		defer watcher.Close()

		rc := &reloader.RemoteConfig{
			ExecName:    c.String("bin"),
			Port:        9222,
			UserDataDir: fmt.Sprintf("%s/.remote-chrome-profile", tmpFolder),
			OriginAddr:  "localhost",
			OriginPort:  c.Int("port"),
			OriginRoute: c.String("route"),
		}
		reloader.RemoteChrome(rc)
		log.Infof("Remote chrome started on port %d", rc.Port)

		go server(c.String("route"), c.Int("port"))
		log.Infof("Server started on port %d with route %s", c.Int("port"), c.String("route"))

		go watch(watcher, rc, shutdown)
		log.Info("Watcher started")

		if c.String("file") != "" {
			filePath := c.String("file")
			if !strings.HasPrefix(filePath, "/") {
				filePath = fmt.Sprintf("%s/%s", c.String("dir"), filePath)
			}
			htmlFile, err := convertToHTML(filePath)
			if err != nil {
				log.Error(err)
			}
			updateViewer(htmlFile, rc)
		}

		// ---- WAITING TO BE KILLED AND REST IN PEACE
		<-sigs
		sigs <- syscall.SIGTERM
		close(shutdown)

		return nil
	}
	app.Run(os.Args)
}
